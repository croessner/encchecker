-- Echo that the test is starting
mt.echo("*** begin test-01 - Send utf-8 header")

-- try to connect to it
conn = mt.connect("inet:4649@127.0.0.1")
if conn == nil then
     error "mt.connect() failed"
end

-- send envelope macros and sender data
-- mt.helo() is called implicitly
mt.macro(conn, SMFIC_MAIL, "i", "test-id")
if mt.mailfrom(conn, "test@example.com") ~= nil then
     error "mt.mailfrom() failed"
end
if mt.getreply(conn) ~= SMFIR_CONTINUE then
     error "mt.mailfrom() unexpected reply"
end

-- send headers
if mt.header(conn, "From", "\"À´×Ôqq.comµÄÍËÐÅ\" <test@example.com>") ~= nil then
     error "mt.header(From) failed"
end
if mt.getreply(conn) ~= SMFIR_REPLYCODE then
     error "mt.header(From) unexpected reply"
end
