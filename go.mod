module encchecker

go 1.24

require (
	github.com/phalaaxx/milter v0.0.0-20240815211724-8304fd4788c2
	github.com/segmentio/ksuid v1.0.4
)
