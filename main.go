/*
encchecker
Copyright 2021 Rößner-Network-Solutions

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/textproto"
	"os"
	"os/signal"
	"syscall"
	"unicode"

	"github.com/phalaaxx/milter"
	"github.com/segmentio/ksuid"
)

var (
	logVerbose  bool
	logOnly     bool
	customReply string

	InfoLogger  *log.Logger
	ErrorLogger *log.Logger
)

type encCheckerMilter struct {
	guid     string
	qid      string
	nonASCII bool

	milter.Milter
}

func init() {
	InfoLogger = log.New(os.Stdout, "INFO: ", 0)
	ErrorLogger = log.New(os.Stdout, "ERROR: ", log.Lshortfile)
}

func (e *encCheckerMilter) MailFrom(_ string, _ *milter.Modifier) (milter.Response, error) {
	e.guid = ksuid.New().String()
	e.qid = ""
	e.nonASCII = false

	return milter.RespContinue, nil
}

func (e *encCheckerMilter) Header(name string, value string, m *milter.Modifier) (milter.Response, error) {
	if len(e.qid) == 0 {
		if val, ok := m.Macros["i"]; ok {
			e.qid = val
		} else {
			e.qid = "-"
		}
	}

	pos, isValidASCII := isASCII(name)
	if !isValidASCII {
		e.nonASCII = true
		name = name[:pos]
	}

	pos, isValidASCII = isASCII(value)
	if !isValidASCII {
		e.nonASCII = true
	}

	if len(name) > 50 {
		name = name[:47] + "..."
	}

	if logVerbose {
		if e.nonASCII {
			InfoLogger.Printf("guid=\"%s\" qid=\"%s\" non_ascii_header=\"%v\" header_name=\"%s\" offset=%d\n",
				e.guid, e.qid, e.nonASCII, name, pos)
		}
	}

	if e.nonASCII && !logOnly {
		return milter.NewResponseStr('y', customReply+fmt.Sprintf("; Header name: %s offset: %d", name, pos)), nil
	}

	if e.nonASCII && logOnly {
		return milter.RespAccept, nil
	}

	return milter.RespContinue, nil
}

func (e *encCheckerMilter) Headers(_ textproto.MIMEHeader, _ *milter.Modifier) (milter.Response, error) {
	if logVerbose {
		if !e.nonASCII {
			InfoLogger.Printf("guid=\"%s\" qid=\"%s\" non_ascii_header=\"%v\"\n", e.guid, e.qid, e.nonASCII)
		}
	}

	return milter.RespContinue, nil
}

// Body is a required callback!
func (*encCheckerMilter) Body(_ *milter.Modifier) (milter.Response, error) {
	return milter.RespContinue, nil
}

func isASCII(s string) (int, bool) {
	for i := 0; i < len(s); i++ {
		if s[i] > unicode.MaxASCII {
			return i, false
		}
	}

	return -1, true
}

func main() {
	// parse commandline arguments
	var protocol, address string

	flag.StringVar(&protocol,
		"proto",
		"tcp",
		"Protocol family (unix or tcp)")
	flag.StringVar(&address,
		"addr",
		"127.0.0.1:4649",
		"Bind to address or unix domain socket")
	flag.BoolVar(&logVerbose,
		"verbose",
		false,
		"Enable verbose output")
	flag.BoolVar(&logOnly,
		"logonly",
		false,
		"Only log results without rejecting emails")
	flag.StringVar(&customReply,
		"reply",
		"554 5.6.0 Reject; strict 7bit header encoding required",
		"Custom response message")

	flag.Parse()

	// Make sure the specified protocol is either unix or tcp
	if protocol != "unix" && protocol != "tcp" {
		ErrorLogger.Fatal("Invalid protocol name")
	}

	if protocol == "unix" {
		// ignore os.Remove errors
		err := os.Remove(address)
		if err != nil {
			ErrorLogger.Fatalln(err)
		}
	}

	if logVerbose {
		InfoLogger.Println("Starting milter encchecker")
	}

	socket, err := net.Listen(protocol, address)
	if err != nil {
		ErrorLogger.Fatal(err)
	}
	//goland:noinspection GoUnhandledErrorResult
	defer socket.Close()

	if protocol == "unix" {
		if err := os.Chmod(address, 0660); err != nil {
			ErrorLogger.Fatal(err)
		}
		//goland:noinspection GoUnhandledErrorResult
		defer os.Remove(address)
	}

	sigs := make(chan os.Signal, 1)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		sig := <-sigs
		if logVerbose {
			InfoLogger.Println("Shutting down. Received signal:", sig)
		}
		os.Exit(0)
	}()

	var NoAction milter.OptAction = 0x0

	// Declare milter init function
	init := func() (milter.Milter, milter.OptAction, milter.OptProtocol) {
		return &encCheckerMilter{},
			NoAction,
			milter.OptNoConnect | milter.OptNoHelo | milter.OptNoRcptTo | milter.OptNoBody
	}
	// Start server
	if err := milter.RunServer(socket, init); err != nil {
		ErrorLogger.Fatal(err)
	}
}
